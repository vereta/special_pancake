Your homework is to create a custom string class. You are free to choose the implementation, but the more methods you provide -- the better. Please also add working examples of usage of these methods.

all good, but couple of notes:
1) you don't need to write this->data or this->len(), access class members directly: data, len() etc. (Done)
2) please add move constructor

Overload next operators (and more if needed) : "=", "+", "+=", "<<", ">>", "==", "!="
Among the standard function-members also add copy/move constructors and next functions:
append
compare
length
resize
clear
swap
substr //should search for a specified substring into existing one and return position of the 1st character
insert //means inserting one character or another string into a specified position

Please create some examples/tests for all these functions
