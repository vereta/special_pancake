#pragma once

#include <cstring>
#include <ostream>
#include <utility>

#define BUFF_SIZE 256

class Str {
    public:
        Str();
        Str(const char *str);
        Str(const Str &str);
        Str(Str &&str);
        Str(const std::string &str);
        Str(size_t n, char c);
        ~Str();

        Str& operator=(const Str& other);
        Str& operator=(const char *str);
        const Str operator+(const Str& other);
        Str& operator+=(const Str& other);

        friend std::ostream &operator<<(std::ostream& out, const Str &str);
        friend std::istream &operator>>(std::istream& in, Str &str);
        friend bool operator== (const Str &lhs, const Str &rhs);
        friend bool operator!= (const Str &lhs, const Str &rhs);

        char &operator[] (size_t index);
        char &back();
        char &front();

        size_t len();
        void clear();
        bool empty();
        void append(const Str& other);
        bool compare(const Str& other);
        static void swap(Str &lhs, Str &rhs);
        void resize(size_t size);
        char *substr(const Str &str);
        Str& insert(size_t index, const char *str);
        Str& insert(size_t index, const char ch);

        char *begin();
        char *end();

    private:
        char *data;
};