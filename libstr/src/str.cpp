#include "str.hpp"
#include <iostream>

Str::Str() : data(nullptr) {
}

Str::Str(const char *str) {
    data = str ? strdup(str) : nullptr;
}

Str::Str(const Str &str) {
    data = str.data ? strdup(str.data) : nullptr;
}

Str::Str(Str &&str) : data(std::exchange(str.data, nullptr)){
}

Str::Str(const std::string& str) : Str::Str(str.c_str()) {
}

Str::Str(size_t n, char c) {
    data = (char*)malloc(sizeof(char) * n + 1);
    data[n] = '\0';
    memset(data, c, n);
}

Str::~Str() {
    if (data){
        free(data);
        data = nullptr;
    }
}

Str& Str::operator=(const Str &other) {
    if (this != &other) {
        if(data) {
            free(data);
        }
        data = other.data ? strdup(other.data) : nullptr;
    }
    return *this;
}

Str& Str::operator=(const char *str) {
    if(data) {
        free(data);
    }

    data = str ? strdup(str) : nullptr;

    return *this;
}

const Str Str::operator+(const Str& other) {
    size_t len1 = data? strlen(data) : 0;
    size_t len2 = other.data ? strlen(other.data) : 0;
    size_t new_len = len1 + len2;
    if(!new_len) {
        return Str();
    }

    Str result(new_len, '\0');

    strncpy(result.data,data,len1);
    strncat(result.data,other.data,len2);

    return result;
}

Str& Str::operator+=(const Str& other) {
    *this =  *this + other;

    return *this;
}

std::ostream& operator<<(std::ostream& out, const Str &str) {
    return str.data ? out << str.data : out << "(null)";
}

std::istream& operator>>(std::istream& in, Str &str) {
    char buffer[BUFF_SIZE] = {0}; //TODO: DEFINE
    in >> buffer;
    str = buffer;
    return in;
}

bool operator==(const Str &lhs, const Str &rhs) {
    if(lhs.data && rhs.data) {
        return !(strcmp(lhs.data, rhs.data));
    }

    if(!lhs.data && !rhs.data) {
        return true;
    }

    return false;
}

bool operator!= (const Str &lhs, const Str &rhs) {
    return !(lhs == rhs);
}

char &Str::operator[] (size_t index) {
    return data[index];
}

/*This function shall not be called on empty strings. */
char &Str::back() {
    return data[len() - 1];
}

/*This function shall not be called on empty strings. */
char &Str::front() {
    return data[0];
}

size_t Str::len() {
    return data ? std::strlen(data) : 0;
}

void Str::clear() {
    if(data) {
        free(data);
        data = nullptr;
    }
    data = strdup("");
}

bool Str::empty() {
    if(len()) {
        return false;
    }
    return true;
}

void Str::append(const Str& other) {
    *this += other;
}

bool Str::compare(const Str& other) {
    return *this == other;
}

void Str::swap(Str &lhs, Str &rhs) {
    Str tmp = lhs;
    lhs = rhs;
    rhs = tmp;
}

void Str::resize(size_t size) {
    Str tmp(size, '\0');
    strncpy(tmp.data, data, size);
    *this = tmp;
}

char *Str::substr(const Str &str) {
    return strstr(data,str.data);
}

Str& Str::insert(size_t index, const char *str) {
    size_t self_len = this->len();
    size_t other_len = strlen(str);
    size_t res_len = self_len + other_len;
    Str result(res_len, '\0');

    strncpy(result.data, data, index);
    strncat(result.data + index,str, other_len);
    strncat(result.data + index + other_len, data + index, self_len);

    *this = result;
    return *this;
}

Str& Str::insert(size_t index, const char ch) {
    Str tmp(1, ch);
    this->insert(index, tmp.data);
    return *this;
}

char *Str::begin() {
    return &(data[0]);
}

char *Str::end() {
    return &(data[this->len()]);
}
