#include "app.hpp"
#include <iostream>
#include <string>

Str create_str(Str str) {
    return str;
}

int main() {
    Str str = "Test";

    //Range-based for
    for(char ch : str) {
        std::cout << ch << std::endl;
    }

    return 0;
}
