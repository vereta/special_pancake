#include <gtest/gtest.h>
#include <string>
#include <sstream>
#include "str.hpp"

Str create_str(Str str) {
    return str;
}

TEST(test_Str, test_default_constructor)
{
    Str str;

    EXPECT_EQ(str.len(), 0);
    EXPECT_TRUE(str.empty());
}

TEST(test_Str, test_parametrized_constructor1)
{
    Str str1("Test");

    EXPECT_EQ(str1.len(), strlen("Test"));
    EXPECT_TRUE(str1 == "Test");
}

TEST(test_Str, test_parametrized_constructor2)
{
    Str str1 = "Test";

    EXPECT_EQ(str1.len(), strlen("Test"));
    EXPECT_TRUE(str1 == "Test");
}

TEST(test_Str, test_copy_constructor1)
{
    Str str1 = "Test";
    Str str2 = str1;

    EXPECT_EQ(str1.len(), str2.len());
    EXPECT_TRUE(str1 == str2);
}

TEST(test_Str, test_copy_constructor2)
{
    Str str1 = "Test";
    Str str2(str1);

    EXPECT_EQ(str1.len(), str2.len());
    EXPECT_TRUE(str1 == str2);
}

TEST(test_Str, test_copy_constructor_string)
{
    std::string str1 = "Test";
    Str str2(str1);

    EXPECT_EQ(str1.length(), str2.len());
    EXPECT_TRUE(str1.c_str() == str2);
}

TEST(test_Str, test_move_constructor)
{
    Str str1 = "Test";
    Str str2 = create_str(Str(str1));

    EXPECT_EQ(str1.len(), str2.len());
    EXPECT_TRUE(str1 == str2);
}

TEST(test_Str, test_fill_constructor)
{
    std::string str1 = "xxxxxxxxxx";
    Str str2(10, 'x');

    EXPECT_EQ(str1.length(), str2.len());
    EXPECT_TRUE(str1.c_str() == str2);
}

TEST(test_Str, test_access1)
{
    Str str1 = "Test";
    Str str2 = "Yest";
    char ch = 'Y';
    size_t pos = 0;

    str1[pos] = ch;

    EXPECT_EQ(str1.len(), str2.len());
    EXPECT_TRUE(str1 == str2);
}

TEST(test_Str, test_access2)
{
    Str str1 = "Test";
    Str str2 = "YesX";
    char front = 'Y';
    char back = 'X';

    str1.front() = front;
    str1.back() = back;

    EXPECT_EQ(str1.len(), str2.len());
    EXPECT_TRUE(str1 == str2);
}

TEST(test_Str, test_len)
{
    Str str1("Test");
    Str str2;

    EXPECT_EQ(str1.len(), strlen("Test"));
    EXPECT_EQ(str2.len(), 0);
}

TEST(test_Str, test_clear_empty)
{
    Str str1("Test");

    EXPECT_EQ(str1.len(), strlen("Test"));
    EXPECT_TRUE(str1 == "Test");
    EXPECT_EQ(str1.empty(), false);

    str1.clear();

    EXPECT_EQ(str1.len(), 0);
    EXPECT_EQ(str1.empty(), true);
    EXPECT_TRUE(str1 == "");
}

TEST(test_Str, test_append_simple)
{
    Str str1("Hello ");
    str1.append("World");

    EXPECT_TRUE(str1=="Hello World");
}

TEST(test_Str, test_append_empty)
{
    Str str1("Hello ");
    str1.append("");

    EXPECT_TRUE(str1=="Hello ");
}

TEST(test_Str, test_append_to_empty)
{
    Str str1;
    str1.append("World");

    EXPECT_TRUE(str1=="World");
}

TEST(test_Str, test_compare_simple1)
{
    Str str1("Test");
    Str str2("Test");

    EXPECT_TRUE(str1.compare(str2));
}

TEST(test_Str, test_compare_simple2)
{
    Str str1("Test1");
    Str str2("Test2");

    EXPECT_FALSE(str1.compare(str2));
}

TEST(test_Str, test_compare_str1)
{
    Str str1("Test");

    EXPECT_TRUE(str1.compare("Test"));
}

TEST(test_Str, test_compare_str2)
{
    Str str1("Test1");

    EXPECT_FALSE(str1.compare("Test2"));
}

TEST(test_Str, test_compare_empty)
{
    Str str1;
    Str str2;

    EXPECT_TRUE(str1.compare(str2));
}

TEST(test_Str, test_swap)
{
    Str str1("Test1");
    Str str2("Test2");

    Str::swap(str1, str2);

    EXPECT_TRUE(str1 == "Test2");
    EXPECT_TRUE(str2 == "Test1");
}

TEST(test_Str, test_resize_simple)
{
    Str str1("Test");
    size_t size = 2;

    str1.resize(size);
    EXPECT_TRUE(str1 == "Te");
    EXPECT_TRUE(str1.len() == size);
}

TEST(test_Str, test_resize_longer)
{
    Str str1("Test");
    size_t size = 10;

    str1.resize(size);
    EXPECT_TRUE(str1 == "Test");
    EXPECT_TRUE(str1.len() == 4);
}

TEST(test_Str, test_substr_true)
{
    Str str1("Finds the first occurrence of the byte string");
    Str str2("the");
    size_t position = 6;

    EXPECT_TRUE(str1.substr(str2) == &(str1[position]));
}

TEST(test_Str, test_substr_false)
{
    Str str1("Finds the first occurrence of the byte string");
    Str str2("XXX");

    EXPECT_FALSE(str1.substr(str2));
}

TEST(test_Str, test_insert_simple)
{
    Str str1("Hello ");

    str1.insert(6, "World");

    EXPECT_TRUE(str1=="Hello World");
}

TEST(test_Str, test_insert_middle)
{
    Str str1("Hello ");

    str1.insert(1, "World");

    EXPECT_TRUE(str1=="HWorldello ");
}

TEST(test_Str, test_insert_start)
{
    Str str1("Hello ");

    str1.insert(0, "World");

    EXPECT_TRUE(str1=="WorldHello ");
}

TEST(test_Str, test_insert_char1)
{
    Str str1("Hello");

    str1.insert(5, '!');

    EXPECT_TRUE(str1=="Hello!");
}

TEST(test_Str, test_insert_char2)
{
    Str str1("Hello");

    str1.insert(0, '!');

    EXPECT_TRUE(str1=="!Hello");
}

TEST(test_Str, test_insert_char3)
{
    Str str1("Hello");

    str1.insert(3, '!');

    EXPECT_TRUE(str1=="Hel!lo");
}

TEST(test_Str, test_operator_equal1)
{
    Str str1("Test");
    Str str2("Test");

    EXPECT_TRUE(str1==str2);
    EXPECT_TRUE(str1=="Test");
    EXPECT_TRUE("Test"==str2);

    EXPECT_FALSE(str1=="str2");

    str2[0] = 'Z';
    EXPECT_FALSE(str1==str2);
}

TEST(test_Str, test_operator_equal2)
{
    Str str1("Test");
    Str str2;

    EXPECT_FALSE(str1 == str2);
}

TEST(test_Str, test_operator_not_equal)
{
    Str str1("Test1");
    Str str2("Test2");

    EXPECT_TRUE(str1 != str2);
    EXPECT_TRUE(str1 != "Test");
    EXPECT_TRUE("Test" != str2);

    EXPECT_FALSE(str1 != "Test1");
    EXPECT_FALSE("Test2" != str2);

    str1.back() = '2';
    EXPECT_FALSE(str1 != str2);
}

TEST(test_Str, test_operator_assignment_char1)
{
    Str str1;

    str1 = "Test";

    EXPECT_TRUE(str1=="Test");
}

TEST(test_Str, test_operator_assignment_char2)
{
    Str str1("Ololo");

    str1 = "Test";

    EXPECT_TRUE(str1=="Test");
}

TEST(test_Str, test_operator_assignment_str1)
{
    Str str1;
    Str str2("Test");

    str1 = str2;

    EXPECT_TRUE(str1==str2);
    EXPECT_TRUE(str1=="Test");
}

TEST(test_Str, test_operator_assignment_str2)
{
    Str str1;
    Str str2;

    str1 = str2;

    EXPECT_TRUE(str1==str2);
    EXPECT_TRUE(str1==NULL);
    EXPECT_TRUE(str1==NULL);
}


 TEST(test_Str, test_operator_assignment_null)
{
    Str str1;
    char *empty = NULL;

    str1 = empty;

    EXPECT_TRUE(str1==NULL);
}

 TEST(test_Str, test_operator_plus_simple)
{
    Str str1("Hello ");
    Str str2("World!");
    Str str3 = str1 + str2;

    EXPECT_TRUE(str3=="Hello World!");
}

 TEST(test_Str, test_operator_plus_str)
{
    Str str1("Hello ");

    Str str3 = str1 + "World!";

    EXPECT_TRUE(str3=="Hello World!");
}

 TEST(test_Str, test_operator_plus_empty1)
{
    Str str1;
    Str str2("World!");
    Str str3 = str1 + str2;

    EXPECT_TRUE(str3=="World!");
}

TEST(test_Str, test_operator_plus_empty2)
{
    Str str1("Hello ");

    Str str3 = str1 + "";

    EXPECT_TRUE(str3=="Hello ");
}

 TEST(test_Str, test_operator_plus_empty3)
{
    Str str1;
    Str str2;
    Str str3 = str1 + str2;

    EXPECT_TRUE(str3==str1);
}

 TEST(test_Str, test_operator_plus_empty4)
{
    Str str1;
    Str str2 = str1 + NULL;

    EXPECT_TRUE(str1==str2);
}

 TEST(test_Str, test_operator_plus_equal_simple)
{
    Str str1("Hello ");
    Str str2("World");

    str1 += str2;

    EXPECT_TRUE(str1=="Hello World");
}

 TEST(test_Str, test_operator_plus_equal_empty)
{
    Str str1;
    Str str2("World");

    str1 += str2;

    EXPECT_TRUE(str1=="World");
}

 TEST(test_Str, test_operator_plus_equal_str)
{
    Str str1("Hello ");

    str1 += "World";

    EXPECT_TRUE(str1=="Hello World");
}

 TEST(test_Str, test_operator_iostream)
{
    Str str1("Test");
    Str str2;
    std::stringstream ss;

    ss << str1;
    ss >> str2;

    EXPECT_TRUE(str1==str2);
}

 TEST(test_Str, test_begin_end)
{
    Str str1("Hello");

    char begin = *str1.begin();
    char end = *str1.end();

    EXPECT_TRUE(begin == 'H');
    EXPECT_TRUE(end == '\0');
}